package entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "user_personal_data", schema = "plane_near_me")
public class UserPersonalData {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "surname")
    private String surname;
    @Basic
    @Column(name = "date_of_birth")
    private Date dateOfBirth;
    @Basic
    @Column(name = "login_id")
    private Integer loginId;
    @ManyToOne
    @JoinColumn(name = "login_id", referencedColumnName = "id")
    private UserLogin userLoginByLoginId;

    public UserPersonalData() {
    }

    public UserPersonalData(int id, String name, String surname, Date dateOfBirth, Integer loginId, UserLogin userLoginByLoginId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.loginId = loginId;
        this.userLoginByLoginId = userLoginByLoginId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPersonalData that = (UserPersonalData) o;
        return id == that.id && Objects.equals(name, that.name) && Objects.equals(surname, that.surname) && Objects.equals(dateOfBirth, that.dateOfBirth) && Objects.equals(loginId, that.loginId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, dateOfBirth, loginId);
    }

    public UserLogin getUserLoginByLoginId() {
        return userLoginByLoginId;
    }

    public void setUserLoginByLoginId(UserLogin userLoginByLoginId) {
        this.userLoginByLoginId = userLoginByLoginId;
    }
}
