package com.example.planenearme;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Connection util used to connect with database via login and password.
 */
public class ConnectionUtil {
    /**
     * User login in database.
     */
    private String userName;
    /**
     * User password in database.
     */
    private String userPassword;

    /**
     * An interface used for connection with database.
     */
    private Connection conn = null;

    /**
     * Constructor with login and password.
     *
     * @param userName     user login
     * @param userPassword user password
     */
    public ConnectionUtil(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    /**
     * Method used to connect to database.
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void dbConnect() throws SQLException, ClassNotFoundException {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "A");
            e.printStackTrace();
            throw e;
        }
        try {
            conn = DriverManager.getConnection(createURL());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Incorrect login or password.");
            e.printStackTrace();
            throw e;
        } catch (InvalidAlgorithmParameterException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method used to disconnect from database.
     *
     * @throws SQLException
     */
    public void dbDisconnect() throws SQLException {

        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
                JOptionPane.showMessageDialog(null, "Disconnected");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Method that generates connection URL based on password and login.
     *
     * @return URL address for connection with database
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     */
    private String createURL() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {

        final String secretKey = "sssshhhh!!!!";
        StringBuilder urlSB = new StringBuilder("jdbc:mysql://");
        urlSB.append("localhost:3306/");
        urlSB.append("plane_near_me?");
        urlSB.append("useUnicode=true&characterEncoding=utf-8");
        urlSB.append("&user=");
        urlSB.append(userName);
        urlSB.append("&password=");
        urlSB.append(AESCipher.encrypt(userPassword, secretKey));
        urlSB.append("&serverTimezone=CET");
        return urlSB.toString();
    }
}
