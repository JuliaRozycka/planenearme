package com.example.planenearme;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Class used to cipher password.
 *
 * @see <a href = "https://howtodoinjava.com/java/java-security/java-aes-encryption-example/">https://howtodoinjava.com/java/java-security/java-aes-encryption-example/</a>
 */
public class AESCipher {
    /**
     * A secret key.
     */
    private static SecretKeySpec secretKey;
    /**
     * Wraps a value of primitive type byte in an object.
     */
    private static byte[] key;

    /**
     * Method used to set a secret key.
     *
     * @param myKey a secret key used to encryption and decryption
     */
    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest();
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method used to encrypt String.
     *
     * @param strToEncrypt provided String
     * @param secret       provided key
     * @return return encrypted String
     */
    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method used to decrypt encrypted String
     *
     * @param strToDecrypt provided String
     * @param secret       provided key
     * @return decrypted String
     */
    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
