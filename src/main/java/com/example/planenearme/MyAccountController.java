package com.example.planenearme;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controller class for fxml file my_account.fxml
 */
public class MyAccountController {

    /**
     * Name field
     */
    @FXML
    private TextField tfName;

    /**
     * Surname field
     */
    @FXML
    private TextField tfSurname;

    /**
     * Date field
     */
    @FXML
    private TextField tfDate;

    /**
     * Login field
     */
    @FXML
    private TextField tfLogin;

    private final int value = StartController.getUserId();

    /**
     * Method called once on an implementing controller when the contents
     * of its associated document have been leaded.
     *
     * @throws  ClassNotFoundException
     * @throws SQLException
     */
    @FXML
    void initialize() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/plane_near_me?useUnicode=true&characterEncoding=utf-8&serverTimezone=CET");

        String sqlGetLogin = "SELECT login_id, name, surname, date_of_birth, login FROM user_view where login_id='" + value + "';";
        Statement st = connection.prepareStatement(sqlGetLogin);
        ResultSet rs = st.executeQuery(sqlGetLogin);
        String name = "";
        String surname = "";
        Date date = null;
        String login = "";

        while (rs.next()) {
            name = rs.getString(2);
            surname = rs.getString(3);
            date = Date.valueOf(rs.getString(4));
            login = rs.getString(5);
        }
        tfName.setText(name);
        tfSurname.setText(surname);
        tfDate.setText(String.valueOf(date));
        tfLogin.setText(login);
        tfLogin.setEditable(false);
    }

    /**
     * Method closes the scene after button click
     *
     * @param event generated action event
     */
    public void btnReturnClick(ActionEvent event) {
        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
    }

    /**
     * Method generated by a button 'Save changes'. Updates chosen
     * rows in the database. For changing personal data.
     *
     * @param event generated action event
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void btnSaveChangesClick(ActionEvent event) throws ClassNotFoundException, SQLException {
        String changedName = tfName.getText();
        String changedSurame = tfSurname.getText();
        String changedDate = tfDate.getText();
        Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
        Matcher matcher = pattern.matcher(changedDate);
        if (!matcher.matches()) {
            JOptionPane.showMessageDialog(null, "Incorrect data input. Should be YYYY-MM-DD");
        } else {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection;
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/plane_near_me?useUnicode=true&characterEncoding=utf-8&serverTimezone=CET");

            String sql = "UPDATE user_personal_data SET name ='" + changedName + "', surname = '" + changedSurame + "', date_of_birth = '" + changedDate + "' WHERE login_id=" + value + ";";
            System.out.println(sql);
            Statement st = connection.createStatement();
            st.executeUpdate(sql);
        }
    }
}
