package com.example.planenearme;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Class used to indirectly read JSON returned by open-sky API.
 * Fields of this class are the keys returned by API.
 */
public class ArrivalsDepartures {

    public String icao24;
    public long firstSeen;
    public String estDepartureAirport;
    public long lastSeen;
    public String estArrivalAirport;
    public String callsign;
    public long estDepartureAirportHorizDistance;
    public long estDepartureAirportVertDistance;
    public long estArrivalAirportHorizDistance;
    public long estArrivalAirportVertDistance;
    public long departureAirportCandidatesCount;
    public long arrivalAirportCandidatesCount;

    /**
     * Constructor with parameters, which are keys returned by API.
     *
     * @param icao24                           24-bit adress
     * @param firstSeen                        time of departure
     * @param estDepartureAirport              departure airport
     * @param lastSeen                         time of arrival
     * @param estArrivalAirport                arrival airport
     * @param callsign                         a unique callsign
     * @param estDepartureAirportHorizDistance
     * @param estDepartureAirportVertDistance
     * @param estArrivalAirportHorizDistance
     * @param estArrivalAirportVertDistance
     * @param departureAirportCandidatesCount
     * @param arrivalAirportCandidatesCount
     */
    public ArrivalsDepartures(String icao24, long firstSeen, String estDepartureAirport, long lastSeen, String estArrivalAirport, String callsign, long estDepartureAirportHorizDistance, long estDepartureAirportVertDistance, long estArrivalAirportHorizDistance, long estArrivalAirportVertDistance, long departureAirportCandidatesCount, long arrivalAirportCandidatesCount) {
        this.icao24 = icao24;
        this.firstSeen = firstSeen;
        this.estDepartureAirport = estDepartureAirport;
        this.lastSeen = lastSeen;
        this.estArrivalAirport = estArrivalAirport;
        this.callsign = callsign;
        this.estDepartureAirportHorizDistance = estDepartureAirportHorizDistance;
        this.estDepartureAirportVertDistance = estDepartureAirportVertDistance;
        this.estArrivalAirportHorizDistance = estArrivalAirportHorizDistance;
        this.estArrivalAirportVertDistance = estArrivalAirportVertDistance;
        this.departureAirportCandidatesCount = departureAirportCandidatesCount;
        this.arrivalAirportCandidatesCount = arrivalAirportCandidatesCount;
    }

    /*public ArrivalsDepartures() {
    }*/

    @Override
    public String toString() {
        return "ArrivalsDepartures{" +
                "icao24='" + icao24 + '\'' +
                ", firstSeen=" + firstSeen +
                ", estDepartureAirport='" + estDepartureAirport + '\'' +
                ", lastSeen=" + lastSeen +
                ", estArrivalAirport='" + estArrivalAirport + '\'' +
                ", callsign='" + callsign + '\'' +
                ", estDepartureAirportHorizDistance=" + estDepartureAirportHorizDistance +
                ", estDepartureAirportVertDistance=" + estDepartureAirportVertDistance +
                ", estArrivalAirportHorizDistance=" + estArrivalAirportHorizDistance +
                ", estArrivalAirportVertDistance=" + estArrivalAirportVertDistance +
                ", departureAirportCandidatesCount=" + departureAirportCandidatesCount +
                ", arrivalAirportCandidatesCount=" + arrivalAirportCandidatesCount +
                '}';
    }

    /**
     * Simple getter for icao24
     *
     * @return iceo 24-bit adress
     */
    public String getIcao24() {
        return icao24;
    }


    /**
     * Simple getter for departure time.
     *
     * @return date and time in format HH:mm (yyyy-MM-dd)
     */
    public String getFirstSeen() {
        return LocalDateTime.ofEpochSecond(firstSeen, 0, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("HH:mm (yyyy-MM-dd)"));
    }

    /**
     * Simple getter for estDepartureAirport
     *
     * @return
     */
    public String getEstDepartureAirport() {
        return estDepartureAirport;
    }

    /**
     * Simple getter for arrival time.
     *
     * @return date and time in format HH:mm (yyyy-MM-dd)
     */
    public String getLastSeen() {
        return LocalDateTime.ofEpochSecond(lastSeen, 0, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("HH:mm (yyyy-MM-dd)"));
    }

    /**
     * Simple getter for estArrivalAirport
     *
     * @return arrival Airport
     */
    public String getEstArrivalAirport() {
        return estArrivalAirport;
    }

    /**
     * Simple getter for callsign
     *
     * @return callsign
     */
    public String getCallsign() {
        return callsign;
    }


    /**
     * Simple getter for estDepartureAirportHorizDistance
     *
     * @return horizontal distance of the last received airborne position to the estimated departure airport in meters
     */
    public long getEstDepartureAirportHorizDistance() {
        return estDepartureAirportHorizDistance;
    }


    /**
     * Simple getter to estDepartureairportVertDistance
     *
     * @return vertical distance of the last received airborne position to the estimated departure airport in meters
     */
    public long getEstDepartureAirportVertDistance() {
        return estDepartureAirportVertDistance;
    }

    /**
     * Simple getter for estArrivalAirportHorizDistance
     *
     * @return Horizontal distance of the last received airborne position to the estimated arrival airport in meters
     */
    public long getEstArrivalAirportHorizDistance() {
        return estArrivalAirportHorizDistance;
    }

    /**
     * Simple getter for estArrivalAirportVertDistance
     *
     * @return vertical distance of the last received airborne position to the estimated arrival airport in meters
     */
    public long getEstArrivalAirportVertDistance() {
        return estArrivalAirportVertDistance;
    }


    /**
     * Simple getter for departureAirportCandidatesCount
     *
     * @return number of other possible departure airports. These are airports in short distance to estDepartureAirport.
     */
    public long getDepartureAirportCandidatesCount() {
        return departureAirportCandidatesCount;
    }

    /**
     * Simple getter for arrivalAirportCandidatesCount
     *
     * @return number of other possible departure airports. These are airports in short distance to estArrivalAirport.
     */
    public long getArrivalAirportCandidatesCount() {
        return arrivalAirportCandidatesCount;
    }

}
