package com.example.planenearme;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.util.Objects;

/**
 * Controller class for searching plane.
 */
public class SearchPlaneController {

    @FXML
    private Label lIcao;
    @FXML
    private Label lCountry;
    @FXML
    private Label lLon;
    @FXML
    private Label lLat;
    @FXML
    private Label lVel;
    @FXML
    private Label lAlt;
    @FXML
    private Label lGro;

    @FXML
    private TextField tfLongitude;

    @FXML
    private TextField tfLatitude;
    @FXML
    private TextField tfLon;
    @FXML
    private TextField tfLat;
    @FXML
    private TextField tfVel;
    @FXML
    private TextField tfCountry;
    @FXML
    private TextField tfIcao;
    @FXML
    private TextField tfAlt;
    @FXML
    private TextField tfGro;

    /**
     * Method calculates and shows which plane is clostest to user
     *
     * @param event action event
     * @throws IOException
     */
    public void btnSearchClicked(ActionEvent event) throws IOException {

        tfIcao.clear();
        tfCountry.clear();
        tfLon.clear();
        tfLat.clear();
        tfVel.clear();
        tfAlt.clear();
        tfGro.clear();

        if (Objects.equals(tfLatitude.getText(), "") || Objects.equals(tfLongitude.getText(), "")) {
            JOptionPane.showMessageDialog(null, "You must enter the data to continue.");
        } else {

            String latString = tfLatitude.getText().replace(",", ".");
            String lonString = tfLongitude.getText().replace(",", ".");
            try {
                double latitude = Double.parseDouble(latString);
                double longitude = Double.parseDouble(lonString);
                if (latitude > 90.0 || latitude < -90.0 || longitude > 180 || longitude < -180) {
                    JOptionPane.showMessageDialog(null, "Incorrect data.\nLatitude = [-90, 90], longitude = [-180, 180].");
                } else {
                    //GetPlaneNearMe getPlaneNearMe = new GetPlaneNearMe();


                    String onGround = "No";
                    if (GetPlaneNearMe.getIcao(latitude, longitude).get(4).equals("true"))
                        onGround = "Yes";

                    tfIcao.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(0)));
                    tfCountry.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(1)));
                    tfLon.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(2)));
                    tfLat.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(3)));
                    tfVel.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(5)));
                    tfAlt.setText(String.valueOf(GetPlaneNearMe.getIcao(latitude, longitude).get(6)));
                    tfGro.setText(onGround);

                    tfIcao.setVisible(true);
                    lIcao.setVisible(true);
                    tfAlt.setVisible(true);
                    lAlt.setVisible(true);
                    tfLon.setVisible(true);
                    lLon.setVisible(true);
                    tfLat.setVisible(true);
                    lLat.setVisible(true);
                    tfCountry.setVisible(true);
                    lCountry.setVisible(true);
                    tfVel.setVisible(true);
                    lVel.setVisible(true);
                    tfGro.setVisible(true);
                    lGro.setVisible(true);
                }

            } catch (NumberFormatException ignore) {
                JOptionPane.showMessageDialog(null, "Wrong input");
            }
        }

    }

    /**
     * Method transfers user to previous tab.
     *
     * @param event action
     * @throws IOException if error occurs while loading
     */
    public void btnReturnClicked(ActionEvent event) throws IOException {

        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
        Parent root = FXMLLoader.load((getClass().getResource("panel_user.fxml")));
        Scene scene = new Scene(root, 500, 350);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
