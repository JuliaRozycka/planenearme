package com.example.planenearme;

import java.util.ArrayList;

/**
 * Class indirectly for reading Json response
 */
public class StateObject {
    public int time;
    public ArrayList<ArrayList<String>> states;

    /**
     * @param time time key
     * @param states states key
     */
    public StateObject(int time, ArrayList<ArrayList<String>> states) {
        this.time = time;
        this.states = states;
    }

    @Override
    public String toString() {
        return "StateObject{" +
                "time=" + time +
                ", states=" + states +
                '}';
    }
}
