package com.example.planenearme;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Class for starting application
 */
public class Start extends Application {
    public static void main(String[] args) {
        launch();
    }

    /**
     * Method opens primary stage
     *
     * @param stage new stage
     * @throws IOException if error occurs while loading new stage
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Start.class.getResource("start.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 430);
        stage.setTitle("PlaneNearMe");
        stage.setScene(scene);
        stage.show();
    }
}