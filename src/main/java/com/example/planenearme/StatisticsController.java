package com.example.planenearme;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Objects;

/**
 * Controller class for statistics table.
 */
public class StatisticsController {

    private final ReadJson readJson = new ReadJson();

    /**
     * Date picker - to
     */
    @FXML
    private DatePicker endDate;

    /**
     * Date picker - from
     */
    @FXML
    private DatePicker startDate;

    /**
     * Table with all columns
     */
    @FXML
    private TableView<ArrivalsDepartures> table;

    @FXML
    private TableColumn<ArrivalsDepartures, String> tcEndAirport;

    @FXML
    private TableColumn<ArrivalsDepartures, String> tcEndTIme;

    @FXML
    private TableColumn<ArrivalsDepartures, String> tcIcao;

    @FXML
    private TableColumn<ArrivalsDepartures, String> tcStartAirport;

    @FXML
    private TableColumn<ArrivalsDepartures, String> tcStartTIme;

    /**
     * Name of airport text field
     */
    @FXML
    private TextField tfAirport;

    /**
     * Method connects with api.
     * Method adds arrivals to the table.
     *
     * @param event action event
     */
    public void showArrivals(ActionEvent event) {

        try {
            long fromDate = startDate.getValue().toEpochSecond(LocalTime.NOON, ZoneOffset.UTC);
            long toDate = endDate.getValue().toEpochSecond(LocalTime.NOON, ZoneOffset.UTC);

            ObservableList<ArrivalsDepartures> dataList = FXCollections.observableArrayList();
            String airport = String.valueOf(tfAirport.getText());
            ArrivalsDepartures[] arrivalsDepartures = readJson.getPlanes("arrival", airport, fromDate, toDate);

            dataList.addAll(Arrays.asList(arrivalsDepartures));
            System.out.println(dataList);

            table.setItems(dataList);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "There are no flights available.");
        }
    }

    /**
     * Method connects with api.
     * Method adds departures to the table.
     *
     * @param event action event
     */
    public void showDepartures(ActionEvent event) {
        if (endDate.getValue().toString() == null || startDate.getValue().toString() == null || Objects.equals(tfAirport.getText(), "")) {
            JOptionPane.showMessageDialog(null, "Please enter data to all the fields.");
        } else {
            try {
                long fromDate = startDate.getValue().toEpochSecond(LocalTime.NOON, ZoneOffset.UTC);
                long toDate = endDate.getValue().toEpochSecond(LocalTime.NOON, ZoneOffset.UTC);

                String airport = tfAirport.getText();

                ObservableList<ArrivalsDepartures> dataList = FXCollections.observableArrayList();
                ArrivalsDepartures[] flights = readJson.getPlanes("departure", airport, fromDate, toDate);

                dataList.addAll(Arrays.asList(flights));

                table.setItems(dataList);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "There are no flights available.");
            }
        }


    }

    /**
     * Method called once on an implementing controller when the contents
     * of its associated document have been leaded.
     *
     * @throws  IOException
     */
    @FXML
    void initialize() throws IOException {

        tcEndAirport.setCellValueFactory(
                new PropertyValueFactory<ArrivalsDepartures, String>("estArrivalAirport")
        );
        tcEndTIme.setCellValueFactory(
                new PropertyValueFactory<ArrivalsDepartures, String>("lastSeen")
        );

        tcIcao.setCellValueFactory(
                new PropertyValueFactory<ArrivalsDepartures, String>("icao24")
        );
        tcStartAirport.setCellValueFactory(
                new PropertyValueFactory<ArrivalsDepartures, String>("estDepartureAirport")
        );
        tcStartTIme.setCellValueFactory(
                new PropertyValueFactory<ArrivalsDepartures, String>("firstSeen")
        );
    }

    /**
     * Method returns user to previous page.
     *
     * @param event action event
     * @throws IOException
     */
    public void btnReturnClicked(ActionEvent event) throws IOException {
        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
        Parent root = FXMLLoader.load((getClass().getResource("panel_user.fxml")));
        Scene scene = new Scene(root, 500, 350);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
