package com.example.planenearme;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class for finding the nearest plane to the given latitude and longitude.
 */
public class GetPlaneNearMe {

    /**
     * Method used to read JSON from URL.
     * Returns Arraylist of Arraylist
     *
     * @param lat given latitude
     * @param lon given longitude
     * @return Arraylist of Arraylists for flights
     * @throws IOException if error occurs while reading data
     */
    public static ArrayList<ArrayList<String>> getPlanes(double lat, double lon) throws IOException {
        String lamin = String.valueOf(lat - 2);
        String lamax = String.valueOf(lat + 2);
        String lomin = String.valueOf(lon - 5);
        String lomax = String.valueOf(lon + 5);

        URL url = new URL("https://kasia1603000:pass@opensky-network.org/api/states/all?lamin=" + lamin + "&lomin=" + lomin + "&lamax=" + lamax + "&lomax=" + lomax);
        StringBuffer response = new StringBuffer();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;

        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        StateObject object = gson.fromJson(response.toString(), StateObject.class);
        return object.states;
    }

    /**
     * Method to get icao from the closest plane.
     *
     * @param lat latitude
     * @param lon longitude
     * @return value of icao
     * @throws IOException if error occurs while reading data
     */
    public static ArrayList<String> getIcao(double lat, double lon) throws IOException {
        ArrayList<ArrayList<String>> list = getPlanes(lat, lon);
        double lo;
        double la;
        double min = Double.POSITIVE_INFINITY;
        String icao = "";
        String country = "";
        String lattitude = "";
        String longnitude = "";
        String onGround = "";
        String velocity = "";
        String geoAltitude = "";
        double R = 6371e3;

        for (ArrayList<String> i : list) {
            lo = Double.parseDouble(i.get(5));
            la = Double.parseDouble(i.get(6));

            double phi1 = la * Math.PI / 180;
            double phi2 = lat * Math.PI / 180;
            double dphi = (lat - la) * Math.PI / 180;
            double dlambda = (lon - lo) * Math.PI / 180;

            double a = Math.sin(dphi / 2) * Math.sin(dphi / 2) +
                    Math.cos(phi1) * Math.cos(phi2) * Math.sin(dlambda / 2) * Math.sin(dlambda / 2);

            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            if (R * c / 1000 < min) {
                min = R * c / 1000;
                icao = i.get(0);
                country = i.get(2);
                longnitude = i.get(5);
                lattitude = i.get(6);
                onGround = i.get(8);
                velocity = i.get(9);
                geoAltitude = i.get(13);
            }
        }
        return new ArrayList<>(Arrays.asList(icao, country, longnitude, lattitude, onGround, velocity, geoAltitude));
    }

}
