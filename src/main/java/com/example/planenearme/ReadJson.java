package com.example.planenearme;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class used to read Json response.
 */
public class ReadJson {

    /**
     * Method used to read response for a specific URL.
     *
     * @param state "arrival" or "departure" depends on what data user want to obtain
     * @param airport airport code
     * @param begin timestamp of departure
     * @param end timestamp od arrival
     * @return array of responses
     * @throws IOException if error occurs while reading
     */
    public ArrivalsDepartures[] getPlanes(String state, String airport, long begin, long end) throws IOException {

        URL url = new URL("https://juliarozycka9616:julia9616@opensky-network.org/api/flights/" + state + "?airport=" + airport + "&begin=" + begin + "&end=" + end);
        System.out.println(url);
        StringBuffer response = new StringBuffer();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;

        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.fromJson(response.toString(), ArrivalsDepartures[].class);
    }

}
